const path = require('path');
const fs = require('fs');
const { execSync } = require('child_process');
const manifest = require('./src/json/manifest.json');

const version = manifest.version;

const name = `prompt_${version}.zip`;

const zipPath = path.resolve('.', 'zips', name);
const extensionPath = path.resolve('.', 'extension');

if (fs.existsSync(zipPath)) {
  console.error(`This zip ${name} already exists`);
  process.exit();
}

try {
  execSync(`zip ${zipPath} -r ${extensionPath}`);
  console.log('Zip complete');
} catch (e) {
  console.error('error while zipping:', e);
  process.exit();
}