/**
 * This finds a place to insert the reader
 * or removes the reader if it needs to
 */


type HookInfo = {
  selector?: string,
  action: 'beforebegin' | 'afterbegin' | 'beforeend' | 'afterend' | 'remove'
}

class HookHelper {
  static insertOrRemove = (element: HTMLElement) => {
    const hostname = window.location.hostname;
    let hookInfo: HookInfo;
    if (hostname.includes('reddit')) {
      hookInfo = HookHelper.getRedditHookInfo()
    } else if (hostname.includes('twitter')) {
      hookInfo = HookHelper.getTwitterHookInfo();
    } else if (hostname.includes('youtube')) {
      hookInfo = HookHelper.getYouTubeHookInfo();
    } else if (hostname.includes('twitch')) {
      hookInfo = HookHelper.getTwitchHookInfo();
    } else if (hostname.includes('tiktok')) {
      hookInfo = HookHelper.getTikTokHookInfo();
    }

    const storedHookInfo = localStorage.getItem('pbHI');

    if (storedHookInfo) {
      try {
        hookInfo = JSON.parse(storedHookInfo);
      } catch (e) {
        console.log('stored hook info didnt parse right');
      }
      console.log('pagey hookinfo', hookInfo)
    }

    if (hookInfo.action === 'remove') {
      element.remove();
      return;
    }
    let target = document.querySelector(hookInfo.selector);
    
    if (!target) {
      element.remove();
      return;
    }

    if (target.parentElement.contains(element)) {
      // no need to add it twice
      return;
    }

    target.insertAdjacentElement(hookInfo.action, element);

  }
  static getRedditHookInfo = (): HookInfo => {
    const href = window.location.href;
    if (href.match(/\/r\//)) {
      return {
        selector: 'div[data-test-id="post-content"]',
        action: 'beforeend',
      };
    } else if (href.match(/\/user\//)) {
      return {
        selector: 'div[data-scroller-first',
        action: 'beforebegin',
      }
    }
    return { action: 'remove' };
  }
  static getTwitterHookInfo = (): HookInfo => {
    const href = window.location.href;
    if (href.match(/\/status\//)) {
      return {
        selector: 'article[tabindex="-1"]',
        action: 'afterend',
      }
    }
    return {
      selector: 'nav[aria-label="Profile timelines"]',
      action: 'beforebegin',
    }
  }
  static getYouTubeHookInfo = (): HookInfo => {
    return {
      selector: '#above-the-fold',
      action: 'afterend',
    }
  }
  static getTwitchHookInfo = (): HookInfo => {
    return {
      selector: '#live-channel-stream-information',
      action: 'afterend',
    }
  }
  static getTikTokHookInfo = (): HookInfo => {
    const href = window.location.href;
    if (href.match(/\/video\/\d+/)) {
      return {
        selector: '[class*="CommentListContainer"',
        action: 'afterbegin',
      }
    } else if (href.match(/\/@/)) {
      return {
        selector: '[data-e2e="user-bio"]', 
        action: 'afterend'
      };
    } else if (href.match(/\/explore/)) {
      return {
        selector: '[class*="DivCategoryListWrapper"]',
        action: 'beforeend',
      }
    }
    return { action: 'remove' };
  }
}

export default HookHelper;