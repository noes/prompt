/**
 * This sends messages to the background
 */

type MessageObject = {
  message: string,
  data?: any,
  requestID?: string
}

class BGCommunicator {
  static send = async (message: MessageObject) => {
    const requestID = Math.random() + '' + Math.random();
    message.requestID = requestID;
    return new Promise<any>((resolve) => {
      const onResponse = (response: any) => {
        // background will send us back the request id
        // so we know this is our resonse
        if (response.requestID === requestID) {
          chrome.runtime.onMessage.removeListener(onResponse);
          resolve(response);
        }
      }
      chrome.runtime.onMessage.addListener(onResponse);
      chrome.runtime.sendMessage(message);
    });
  }
}

export default BGCommunicator;