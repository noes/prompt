/**
 * This module is used to retrieve webring data from storage
 * and watch for storage changes
 */

export interface WebringData {
  subreddits: {
    [key:string]: string[]
  },
  webrings: {
    [key:string]: {
      members: string,
      image: string | undefined,
    }
  }
}

export type StorageChangeHandler = (webringData: WebringData) => void;

class Storage {
  changeCallback: StorageChangeHandler;
  constructor(callback: StorageChangeHandler) {
    this.changeCallback = callback;
  }
  /**
   * Make initial check for webrings in storage
   * and sets up change listener
   */
  init = async () => {
    const webrings = await this.checkStorage();
    if (webrings) {
      this.changeCallback(webrings as WebringData);
    }
    chrome.storage.onChanged.addListener((changes) => {
      // we only care about changes to webrings
      if (changes.webrings) {
        this.changeCallback(changes.webrings.newValue as WebringData);
      }
    })
  }
  checkStorage = () => {
    return new Promise((resolve) => {
      chrome.storage.local.get(['webrings'], (results) => {
        resolve(results.webrings);
      })
    });
  }
}

export default Storage;