/**
 * @description Just a simple debounce function
 * @param func The function to debounce
 * @param waitTime the wait time to debounce
 */

const debounce = (
  func: (...args: any[]) => void, 
  waitTime: number
): ((...args: any[]) => void) => {
  let timer: NodeJS.Timeout|undefined;
  return (...subArgs: any[]): void => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(null, subArgs);
    }, waitTime);
  }
}

export default debounce;