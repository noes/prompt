import debounce from '../debounce';

const wait = (waitTime: number) => new Promise(r => setTimeout(r, waitTime));

describe('testing debounce', () => {
  test('testing rapid firings', async () => {
    let num = 0;
    let increment = debounce(() => num++, 100);

    for (let i = 0; i < 100; i++) {
      increment();
    }

    expect(num).toBe(0);

    await wait(150);

    expect(num).toBe(1);

    increment();

    await wait(150);

    expect(num).toBe(2);
  })
})