
/**
 * @function getAssetUrl
 * @description This grabs the url to an asset in the 
 * extension's files and then fetches it
 * @param {string} assetPath The path to the asset in extension
 * @returns {string} The extension url
 */
const getAssetUrl = (assetPath: string): string => {
  const browser = (globalThis as any).browser ?? globalThis.chrome;
  const extensionURL = browser.runtime.getURL(assetPath);
  if (!extensionURL) {
    throw new Error('Error getting extensionURL: assetPath didnt point to anything')
  }
  return extensionURL;
}

export default getAssetUrl;