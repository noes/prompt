import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components';

type ModalSheetProps = {
  isShowing: boolean
}

const ModalSheet = styled.div<ModalSheetProps>`
  ${({ isShowing }) => {
    return isShowing ? `
      opacity: 1;
    ` : `
      opacity: 0;
      pointer-events: none;
    `;
  }}
  display: flex;
  width: 100vw;
  height: 100vh;
  align-items: center;
  justify-content: center;
  background-color: rgba(0,0,0,.4);
  position: fixed;
  z-index: 1000;
  top: 0;
  left: 0;
  transition: opacity .25s;
`;

const ModalBox = styled.div`
  width: 75%;
  max-width: 600px;
  border-radius: 5px;
  background-color: white;
  color: #3c3c3c;
  font-size: 13px;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  padding: 5px 14px;
`;

const HeaderInput = styled.input`
  font-weight: bold;
  font-family: arial;
  flex-grow: 1;
  color: inherit;
  outline: none;
`;

const AllButton = styled.button`
  border: 1px solid #759748;
  color: #759748;
  font-size: inherit;
  padding: 5px;
`;

const SandwichButton = styled.button`
  font-size: inherit;
  padding: 5px;
  margin-left: 5px;
`;

const PromptsSubheader = styled.div`
  background-color: #fbfbfd;
  font-size: 9px;
  color: grey;
  padding: 5px 14px;
  text-transform: uppercase;
`;

const PromptsListDetailWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const PromptsList = styled.div`
  overlow-y: auto;
  flex-grow: 1;
`;

const PromptListItem = styled.div`
  border-left: 4px solid transparent;
  padding-bottom: 10px;
  cursor: pointer;
  &:hover {
    border-left: 4px solid #83a571;
    background-color: #efefef;
  }
`;

const PromptListItemTitle = styled.h2`
  font-size: inherit;
  padding: 5px 10px;
  line-height: 1;
  pointer-events: none;
`;

const PromptListItemBody = styled.p`
  height: 40px;
  overflow: hidden;
  padding: 5px 10px;
  pointer-events: none;
`;

const PromptsDetail = styled.div`
  width: 40%;
  flex-shrink: 0;
`;

const PromptsDetailText = styled.p`
  background-color: #f9f8f4;
  height: 300px;
  padding: 5px 10px;
`;
const UsePromptButton = styled(AllButton)<{ active: boolean }>`
  margin: 10px;
  width: calc(100% - 20px);
  ${({ active }) => {
    return active ? `
      visibility: visible;
    ` : `
      visibility: hidden;
    `;
  }}
`;

const prompts = [
  {
    id: 0,
    title: 'Prompt 1',
    prompt: 'adasdf adsfjadsfaksdfjakdsfjkasdfjasdf sdfasdf adf asdfasf.adasdf adsfjadsfaksdfjakdsfjkasdfjasdf sdfasdf adf asdfasf.adasdf adsfjadsfaksdfjakdsfjkasdfjasdf sdfasdf adf asdfasf.adasdf adsfjadsfaksdfjakdsfjkasdfjasdf sdfasdf adf asdfasf.'
  },
  {
    id: 1,
    title: 'Prompt 2',
    prompt: 'vetweotiwotiwetoiwe wet iqowe tiwe qweitqowet oweti owet oqweitvetweotiwotiwetoiwe wet iqowe tiwe qweitqowet oweti owet oqweitvetweotiwotiwetoiwe wet iqowe tiwe qweitqowet oweti owet oqweitvetweotiwotiwetoiwe wet iqowe tiwe qweitqowet oweti owet oqweit'
  },
  {
    id: 2,
    title: 'Prompt 3',
    prompt: 'zxczxcvzc zxczxcv zxcv zxcv zxcv zxcv zcxv zv zxcv zxcv zcxv zxczxcvzc zxczxcv zxcv zxcv zxcv zxcv zcxv zv zxcv zxcv zcxv zxczxcvzc zxczxcv zxcv zxcv zxcv zxcv zcxv zv zxcv zxcv zcxv zxczxcvzc zxczxcv zxcv zxcv zxcv zxcv zcxv zv zxcv zxcv zcxv ',
  },
  {
    id: 3,
    title: 'Prompt 4',
    prompt: '22235236032 0230 230 0230230 230203023 0230620360223-236023022235236032 0230 230 0230230 230203023 0230620360223-236023022235236032 0230 230 0230230 230203023 0230620360223-236023022235236032 0230 230 0230230 230203023 0230620360223-2360230',
  }
]

const Modal: React.FC = () => {
  const [showing, setShowing] = useState<boolean>(false);
  const [userInput, setUserInput] = useState<string>('');
  const [selectedPrompt, selectPrompt] = useState<number>(undefined);
  const inputRef = useRef();
  
  useEffect(() => {
    document.body.addEventListener('keydown', (e) => {
      if (e.key === 'k' && (e.metaKey || e.ctrlKey)) {
        e.preventDefault();
        setShowing(true);
        (inputRef.current as HTMLInputElement).focus();
      }
    })
  }, []);

  const close = () => setShowing(false);
  const catchClick = (e: React.MouseEvent) => e.stopPropagation(); 

  let promptOptions = userInput ? prompts : [];

  const prompt = prompts.find((pr) => pr.id === selectedPrompt);

  const choosePrompt = () => {
    const textArea = (document.querySelector('#prompt-textarea') as HTMLTextAreaElement);
    textArea.value = prompt.prompt;
    const event = new Event('input', {
      bubbles: true,
      cancelable: true,
    });
    textArea.dispatchEvent(event)
    setShowing(false);
  }

  return (
    <ModalSheet isShowing={showing} onClick={close}>
      <ModalBox onClick={catchClick}>
        <Header>
          <HeaderInput ref={inputRef} value={userInput} onChange={(e)=> setUserInput(e.target.value)}/>
          <AllButton>ALL</AllButton>
          <SandwichButton>☰</SandwichButton>
        </Header>
        <PromptsSubheader>Prompts</PromptsSubheader>
        <PromptsListDetailWrapper>
          <PromptsList>
            {
              promptOptions.map((opt) => {
                return (
                  <PromptListItem key={opt.id} onClick={() => selectPrompt(opt.id)}>
                    <PromptListItemTitle>{opt.title}</PromptListItemTitle>
                    <PromptListItemBody>{opt.prompt}</PromptListItemBody>
                  </PromptListItem>
                )
              })
            }
          </PromptsList>
          <PromptsDetail>
            <PromptsDetailText>
              { prompt && prompt.prompt }
            </PromptsDetailText>
            <UsePromptButton active={!!prompt} onClick={choosePrompt}>Use This Prompt</UsePromptButton>            
          </PromptsDetail>
        </PromptsListDetailWrapper>
      </ModalBox>
    </ModalSheet>
  )
}

export default Modal;