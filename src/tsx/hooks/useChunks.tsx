import react, { useState, useCallback } from 'react';
import usePoll from 'hooks/usePoll';

type ChunkData = {
  text: string[],
  url: string,
  chunkNumber: number,
  totalChunks: number,
  changeChunk: (value: number | boolean) => void,
  resetAll: () => void,
}

const useChunks = (): ChunkData => {
  const [id, setId] = useState<string>('');
  const [url, setUrl] = useState<string>('');
  const [chunk, setChunk] = useState<number>(0);
  const [chunks, setChunks] = useState<Array<string[]>>([]);
  const [settingChunk, setSettingChunk] = useState<boolean>(false);

  const checkChunks = useCallback(async () => {
    if (settingChunk) {
      return;
    }
    const sId = await chrome.storage.local.get(['id']);
    if (sId.id !== id) {
      console.log('new id')
      // the text has changed
      setId(sId.id);
      setChunk(0);
      setChunks([])
      const sItems = await chrome.storage.local.get(['url', 'chunk', 'chunks']);
      setUrl(sItems.url);
      setChunk(sItems.chunk);
      setChunks(sItems.chunks);
    } else {
      // chunk can change even if id doesn't change
      // because user can change chunk on other tab
      const sChunk = await chrome.storage.local.get(['chunk']);
      if (sChunk.chunk !== chunk) {
        setChunk(sChunk.chunk);
      }
    }
  }, [id, url, chunk, chunks, settingChunk]);

  usePoll(500, checkChunks);

  const text = (chunks?.[chunk] || [])

  // if value is true, then it means decrement
  // if value is false, then it means increment
  // if value is a number, then it means a chunk number
  const changeChunk = useCallback(async (value: number | boolean) => {
    setSettingChunk(true);

    let newChunk: number;
    if (typeof value === 'number' && value !== NaN) {
      newChunk = value;
    } else if (typeof value === 'boolean') {
      const diff = value ? -1 : 1;
      newChunk = chunk + diff;
    }

    if (newChunk < 0) {
      newChunk = chunks.length - 1;
    } else if(newChunk >= chunks.length) {
      newChunk = 0;
    }
    setChunk(newChunk);
    await chrome.storage.local.set({ chunk: newChunk })
    setSettingChunk(false);
  }, [chunk, chunks])

  const resetAll = useCallback(async () => {
    setSettingChunk(true);
    await chrome.storage.local.remove(
      ['id', 'url', 'chunk', 'chunks']
    );
    setSettingChunk(false);
  }, []);

  return {
    text, // text of chunk
    url,  // url of page that chunk came from
    chunkNumber: chunk + 1, // the 1-indexed chunk number
    totalChunks: chunks?.length || 0, // total chunks,
    changeChunk, // callback for changing chunk number,
    resetAll, // reset all data,k
  };

}

export default useChunks;