import react, { useRef, useEffect } from 'react';

/**
 * A hook for setting up a poll
 * @param millis 
 * @param callback 
 */
const usePoll = (millis: number, callback: () => {}) => {
  const callbackRef = useRef(callback);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback])

  useEffect(() => {
    const intervalId = setInterval(() => {
      if (callbackRef.current) {
        callbackRef.current();
      }
    }, millis);
    return () => clearInterval(intervalId);

  }, [millis]);

}

export default usePoll;