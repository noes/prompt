import { createRoot } from 'react-dom/client';
import React from 'react';
import App from 'components/Modal/Modal';

const rootElement = document.createElement('div');
rootElement.id = 'promptRoot';
document.body.appendChild(rootElement);

const root = createRoot(rootElement);

root.render(<App/>);