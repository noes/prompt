const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = (env) => ({
  mode: env.mode,
  devtool: "source-map",
  entry: {
    "js/prompt-content": path.resolve(__dirname, "src", "tsx", "content-entry.tsx"),
  },
  output: {
    path: path.resolve(__dirname, "extension"),
    filename: "[name].js",
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          { loader: "ts-loader" }
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "images",
        },
      },
      {
        test: /\.(ttf|otf)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts",
        },
      }
    ]
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, "src", "json", "manifest.json"),
          to: path.resolve(__dirname, "extension", "manifest.json"),
        },
        {
          from: path.resolve(__dirname, "src", "js", 'service-worker.js'),
          to: path.resolve(__dirname, "extension", "js", "service-worker.js"),
        },
        {
          from: path.resolve(__dirname, "src", "assets"),
          to: path.resolve(__dirname, "extension", "assets"),
        },
      ],
    }),
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    alias: {
      modules: path.join(__dirname, "src", "ts", "modules"),
      extension: path.join(__dirname, "src", "ts", "extension"),
      helpers: path.join(__dirname, "src", "ts", "helpers"),
      types: path.join(__dirname, "src", "ts", "types"),
      components: path.join(__dirname, "src", "tsx", "components"),
      hooks: path.join(__dirname, 'src', 'tsx', 'hooks'),
    }
  }
})